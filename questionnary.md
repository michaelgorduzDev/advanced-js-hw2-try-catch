Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. 

try {
  let jsonData = JSON.parse(jsonString);
} catch (error) {
}

`````````````````````````````````
2.

try {
  const fileContents = readFile(fileName);
  // Code that processes the file contents
} catch (error) {
  // Handling errors that occur while reading or processing the file
}

``````````````````````````````````
3.

try {
  const userData = database.query('SELECT * FROM users');
  // Code that works with the retrieved user data
} catch (error) {
  // Handling errors related to database access or query execution
}


