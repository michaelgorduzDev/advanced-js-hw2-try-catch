const books = [
    {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70
    },
    {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    },
    {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    },
    {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40
    },
    {
      author: "Анґус Гайленд",
      name: "Коти в мистецтві",
    }
  ];
  
  const ul = document.createElement("ul");
  
  books.forEach((book) => {
    try {
      // Check if the book object has all three properties: author, name, and price
      if (book.author && book.name && book.price) {
        const li = document.createElement("li");
  
        // Set the text content of the <li> element
        li.textContent = `${book.author}: ${book.name}, ${book.price} USD`;
        ul.appendChild(li);
      } else {
        throw new Error("Invalid book object");
      }
    } catch (error) {
      // Log an error message to the console
      if (!book.author) {
        console.error("Missing 'author' property in book object:", book);
      }
      if (!book.name) {
        console.error("Missing 'name' property in book object:", book);
      }
      if (!book.price) {
        console.error("Missing 'price' property in book object:", book);
      }
    }
  });
  
  document.getElementById("root").appendChild(ul);
  